package com.rest.demo.services;

import com.rest.demo.entities.Student;
import com.rest.demo.exceptions.StudentNotFoundException;

import java.util.List;
import java.util.Optional;


public interface StudentService {

    List<Student> getAllStudents();
    Optional<Student> getStudentById(Long id) throws StudentNotFoundException;
    Optional <Student> create(Student student);
    Optional <Student> update(Student student, Long id)  throws StudentNotFoundException;
    void deleteStudentById(Long id) throws StudentNotFoundException;

}
