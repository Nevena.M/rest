package com.rest.demo.services;

import com.rest.demo.entities.Student;
import com.rest.demo.exceptions.StudentNotFoundException;
import com.rest.demo.repositories.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;


@Service
public class StudentServiceImpl implements StudentService {

    public StudentServiceImpl() {
    }

    @Autowired
    private StudentRepository studentRepository;

    private Optional <Student> studentS;

    @Override
    @Transactional
    public List<Student> getAllStudents() {
        List<Student> list = studentRepository.findAll();
        return list;
    }

    @Override
    @Transactional
    public Optional <Student> getStudentById(Long id)  throws StudentNotFoundException{
         studentS = studentRepository.findById(id);
        if (!studentS.isPresent()) {
            throw new StudentNotFoundException("Student doesn't exist!");
        }

        return  studentS;

    }

    @Override
    @Transactional
    public Optional <Student> create(Student student) {
        Student studentS =  studentRepository.save(student);
        return Optional.of(studentS);

    }

    @Override
    @Transactional
    public Optional <Student> update(Student student, Long id)  throws StudentNotFoundException {
        studentS = studentRepository.findById(id);

        if (!studentS.isPresent()) {
            throw new StudentNotFoundException("Student doesn't exist!");
        }
        student.setId(id);
        studentRepository.save(student);
        studentS = studentRepository.findById(id);

        return studentS;


    }

    @Override
    @Transactional
    public void deleteStudentById(Long id)  throws StudentNotFoundException{
        studentS = studentRepository.findById(id);
        if (!studentS.isPresent()) {
            throw new StudentNotFoundException("Student doesn't exist!");
        }
        studentRepository.deleteById(id);

    }


}

