package com.rest.demo.test;


import com.rest.demo.entities.Student;
import com.rest.demo.exceptions.StudentNotFoundException;
import com.rest.demo.repositories.StudentRepository;
import com.rest.demo.services.StudentServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class StudentServiceTest {

    @Mock
    private StudentRepository studentRepository;

    @InjectMocks
    private StudentServiceImpl studentServiceImpl;

    @Before
    public void setup(){
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testGetAllStudents(){
        //prepare
        List<Student> studentList = new ArrayList<>();
        studentList.add(new Student(11L,"Student Sample 1","1234"));
        studentList.add(new Student(8L,"Todo Sample 2", "2345"));
        studentList.add(new Student(9L,"Todo Sample 3", "3456"));
        when(studentRepository.findAll()).thenReturn(studentList);

        //exercise
        List<Student> result = studentServiceImpl.getAllStudents();

        //verify
        assertEquals(3, result.size());


    }

    @Test
    public void testGetStudentById() throws StudentNotFoundException{
        //prepare
        Student student = new Student(4L,"Student 1", "3456");
        when((studentRepository.findById(4L))).thenReturn(Optional.ofNullable(student));

        //exercise
        Student result = studentServiceImpl.getStudentById(4L).get();

        //verify
        assertEquals(student, result);
    }

    @Test
    public void testCreateStudent() throws StudentNotFoundException {
        //prepare
        Student student = new Student(1L,"Student 2","3456");
        when(studentRepository.save(student)).thenReturn(student);

        //exercise
        Student result = studentServiceImpl.create(student).get();

        //verify
        assertEquals(student, result);
    }

    @Test
    public void testUpdateStudent() throws StudentNotFoundException {
        //prepare
        Student student = new Student(9L, "Student 3", "6789");
        Student student1 = new Student(5L, "Student 4", "6789");

        when(studentRepository.findById(9L)).thenReturn(Optional.ofNullable(student));
        when(studentServiceImpl.update(student1, 9L)).thenReturn(Optional.ofNullable(student1));

        //exercise
        Student result = studentServiceImpl.update(student1, 9L).get();

        //verify
        assertEquals(9L, result.getId().longValue());
        assertEquals("Student 4", result.getName());
        assertEquals("6789", result.getPassportNumber());
    }

        @Test
    public void testDeleteStudentById() throws StudentNotFoundException{
        //prepare
        Student student = new Student(8L,"Student 3","5678");
        when((studentRepository.findById(8L))).thenReturn(Optional.ofNullable(student));

        //exercise
        studentServiceImpl.deleteStudentById(student.getId());


        //verify
        verify(studentRepository, times(1)).deleteById(student.getId().longValue());
    }


}
